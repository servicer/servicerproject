function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">logi välja</a>`;
    }
    document.getElementById("topMenuContainer").innerHTML = /*html*/`
        <div style="padding: 5px; color: white; font-style: italic; background:black;">
            <div class="row">
                <div class="col-6">
                    <strong>Vali IT!</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal;">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}
function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}
function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}
function showOccupationList() {
    document.getElementById("occupationList").style.display = "block";
    document.getElementById("clientList").style.visibility = "none"
    loadOccupations();
}
function changeView() {
    if(document.getElementById("viewSelect").value === "LIST2") {
        showOccupationList();
    } else {
        showClientList();
    }
}
function showClientList() {
    document.getElementById("clientList").style.display = "block";
    document.getElementById("occupationList").style.visibility = "none"
    loadClients();
}

