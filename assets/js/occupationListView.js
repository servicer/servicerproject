function generateOccupationList(occupations) {
    let rows = "";
    for(let i = 0; i < occupations.length; i++) {
        rows += `
            ${generateOccupationRowHeading(occupations[i])}
            ${generateOccupationRowElement(
                'Nimi', occupations[i].name != null ? occupations[i].name : 'info puudub',
            )}
            ${generateOccupationRowElement(
                'Kirjeldus', occupations[i].description != null ? occupations[i].description : 'info puudub',
            )}
            ${generateOccupationRowButtons(occupations[i])}
        `;
    }
    document.getElementById("occupationList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
                <div class="row">
                    <div class="col-12" style="padding: 5px;">
                        <button class="btn btn-success btn-block" onClick="openOccupationModalForInsert()">Lisa amet</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}
function generateOccupationRowHeading(occupation) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 28px;">${occupation.name}</strong>
            </div>
        </div>
    `;
}
function generateOccupationRowElement(cell1Content, cell2Content) {
    return /*html*/`
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateOccupationRowTitleCell(cell1Content)}
                    ${generateOccupationRowValueCell(cell2Content)}
                </div>
            </div>
        </div>
    `;
}
function generateOccupationRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}
function generateOccupationRowValueCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}
function generateOccupationRowButtons(occupation) {
    return /*html*/`
        <div class="row justify-content-left">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openOccupationModalForUpdate(${occupation.id})">Muuda</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeOccupation(${occupation.id})">Kustuta</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}
function openOccupationModal() {
    $("#occupationModal").modal('show');
}
function closeOccupationModal() {
    $("#occupationModal").modal('hide');
}
function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}
function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}
function clearOccupationModal() {
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("description").value = null;
}
function fillOccupationModal(occupation) {
    document.getElementById("id").value = occupation.id;
    document.getElementById("name").value = occupation.name;
    document.getElementById("description").value = occupation.description;
}