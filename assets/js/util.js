// Validation functions. Siia panen need funktsioonid mis on loogilised tükid ja teevad konkreetsed asja ja mis pole kontrolleris
function validateClientModal(client) {
    if (isEmpty(client.name)) {
        showError("Nimi on kohustuslik!")
        return false;
    }
    return true;
}
function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}
function isEmpty(text) {
    return (!text || 0 === text.length);
}
// DOM retrieval functions
function getFileInput() {
    return document.getElementById("photo");
}
function getClientFromModal() {
    return {
        "id": document.getElementById("id").value,
        "name": document.getElementById("name").value,
        "photo": document.getElementById("photo").value,
        "description": document.getElementById("description").value,
    };
}
function getOccupationFromModal() {
    return {
        "id": document.getElementById("id").value,
        "name": document.getElementById("name").value,
        "description": document.getElementById("description").value,
    };
}
function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}