async function fetchClients() {
    try {
        let response = await fetch(
            `${API_URL}/clients`,
            {
                metohd: "GET",
                headers: {
                    "Authorization": `Bearer ${getToken()}`
                }
            }
        );
        checkResponse(response);
        return await response.json();
    } catch (e) {
        return Promise.reject("Request failed: " + e);
    }
}
function fetchClient(id) {
    return fetch(
        `${API_URL}/clients/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(client => client.json());
}
function uploadPhoto(photo) {
    let formData = new FormData();
    formData.append("photo", photo);
    return fetch(
            `${API_URL}/photos/upload`,
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}
function postClient(client) {
    return fetch(
        `${API_URL}/clients`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(client)
        }
    )
    .then(checkResponse);
}
function deleteClient(id) {
    return fetch(
        `${API_URL}/clients/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}
function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}
function checkResponse(response) {
    if (!response.ok) {
  //      clearAuthentication();
   //     showLoginContainer();
        closeClientModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}