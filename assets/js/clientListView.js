function generateClientList(clients) {
    let rows = "";
    for(let i = 0; i < clients.length; i++) {
        rows += `
            ${generateClientRowHeading(clients[i])}
            ${generateClientRowElement(
                'Nimi', clients[i].name != null ? clients[i].name : 'info puudub',
            )}
            ${generateClientRowElement(
                'Kirjeldus', clients[i].description != null ? clients[i].description : 'info puudub',
            )}
            ${generateClientRowButtons(clients[i])}
        `;
    }
    document.getElementById("clientList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-11">
                ${rows}
                <div class="row">
                    <div class="col-12" style="padding: 5px;">
                        <button class="btn btn-success btn-block" onClick="openClientModalForInsert()">Lisa klient</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}
function generateClientRowHeading(client) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                ${generateImageElement(client.photo)}
            </div>
        </div>
    `;
}
function generateClientRowElement(cell1Content, cell2Content) {
    return /*html*/`
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateClientRowTitleCell(cell1Content)}
                    ${generateClientRowValueCell(cell2Content)}
                </div>
            </div>
        </div>
    `;
}
function generateClientRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}
function generateClientRowValueCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}
function generateClientRowButtons(client) {
    return /*html*/`
        <div class="row justify-content-left">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openClientModalForUpdate(${client.id})">Muuda</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeClient(${client.id})">Kustuta</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}
function generateImageElement(photo) {
    if (!isEmpty(photo)) {
        return /*html*/`<img src="${photo}" width="150" />`;
    } else {
        return "[PILT PUUDUB]";
    }
}
function openClientModal() {
    $("#clientModal").modal('show');
}
function closeClientModal() {
    $("#clientModal").modal('hide');
}
function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}
function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}
function clearClientModal() {
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("photo").value = null;
    document.getElementById("description").value = null;
}
function fillClientModal(client) {
    document.getElementById("id").value = client.id;
    document.getElementById("name").value = client.name;
    document.getElementById("photo").value = client.photo;
    document.getElementById("description").value = client.description;
}
function fillClientModalPhotoField(photo) {
    document.getElementById("photo").value = photo;
}