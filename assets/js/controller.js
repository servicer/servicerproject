function loadClients() {
    fetchClients().then(generateClientList);
}
//function loadOccupations() {
  //  fetchOccupations().then(generateOccupationList);
//}
function openClientModalForUpdate(id) {
    clearError();
    fetchClient(id).then(client => {
        openClientModal();
        clearClientModal();
        fillClientModal(client);
    });
}
function openClientModalForInsert() {
    openClientModal();
    clearError();
    clearClientModal();
}
function saveFile(event) {
    event.preventDefault();
    let FileInput = getFileInput();
    uploadFile(FileInput.files[0]).then(response => fillClientModalFileField(response.url));
}
function saveClient() {
    let client = getClientFromModal();
    if (validateClientModal(client)) {
        postClient(client)
            .then(() => {
                closeClientModal();
                loadClients();
            });
    }
}
function removeClient(id) {
    if(confirm('Soovid kasutajanime kustutada?')) {
        deleteClient(id).then(loadClients)
    }
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadClients();
        })
    }
}
function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}